class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.datetime :date
      t.text :reason
      t.float :fee

      t.timestamps
    end
  end
end
